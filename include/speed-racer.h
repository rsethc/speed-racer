#pragma once

#include <vector>

typedef void* (RacingFunc) (void* arg);

struct Race
{
    std::vector<RacingFunc*> funcs;
    RacingFunc Go;
};

struct RacingFuncsException {};
struct RacingFuncsEmpty : public RacingFuncsException {};
struct RacingFuncsAllFailed : public RacingFuncsException {};
