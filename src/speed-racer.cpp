#include "speed-racer.h"

#include <mutex>
#include <memory>

using namespace std;

struct RaceState
{
    void* arg;
    void* result; // To do: template-ize this instead of using void*'s
    bool posted;
    mutex modify_mutex;
    semaphore result_post;
    int ongoing;
    void PostResult (void* incoming_result)
    {
        lock_guard<mutex> lock(modify_mutex);
        ongoing--;
        if (!posted)
        {
            posted = true;
            result = incoming_result;
            result_post.post();
        }
    }
    void PostFail ()
    {
        lock_guard<mutex> lock(modify_mutex);
        ongoing--;
        if (!ongoing && !posted)
        {
            // All threads failed, wake up the one who started the race
            // and it will receive NULL.
            result = NULL;
            result_post.post();
        }
    }
};

void DoRacingFunc (RacingFunc* func, shared_ptr<RaceState> state)
{
    try
    {
        void* result = func(state->arg);
        state->PostResult(result);
    }
    catch (...)
    {
        state->PostFail();
    }
}

void* Race::Go (void* arg)
{
    if (!funcs.size())
    {
        throw RacingFuncsEmpty {};
    }

    shared_ptr<RaceState> state { new RaceState };
    state->arg = arg;
    state->ongoing = funcs.size();
    for (RacingFunc* func : funcs)
    {
        thread {DoRacingFunc, func, state}.detach();
    }

    state.result_post.take();
    if (state->posted)
    {
        return state.result;
    }
    else
    {
        throw RacingFuncsAllFailed {};
    }
}
